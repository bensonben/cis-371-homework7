var request = $.ajax({
    url: "http://www.cis.gvsu.edu/~scrippsj/cs371/hw/hw07/getTitles.php",
    type: "GET",
    dataType: "JSON",
    success: function(response) {
        for (var i = 1; i <= response.length; i++) {
            $("#movies").append('<option value="' + i + '">' + response[i - 1] + '</option>');

        }
        choose();
        $("#movies").change(choose);
    },
    error: function(exception) { alert('An error occurred'); }
});
$(function() {
    $("#tabs").tabs();
});

function choose() {
    var index = $("#movies").val()
    var request2 = $.ajax({
        url: "http://www.cis.gvsu.edu/~scrippsj/cs371/hw/hw07/getMovie.php",
        type: "GET",
        data: { id: index },
        success: function(response) {
            var data = response.split("\t");
            $("#summary").empty().append("<h3>" + data[0] + "</h3>" + data[1]);
            $("#cast").empty().append(data[2]);
        },
        error: function(exception) { alert('An error occurred'); }
    });
}
